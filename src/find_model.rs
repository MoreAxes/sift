use sift;
use sift::Point;
use util::*;

use std::error::Error;
use std::f64;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::str::FromStr;

use abomonation::{encode, decode};

use clap;

use rand;
use rand::SeedableRng;

use time;

pub fn go(db_path: &str, args: clap::ArgMatches) -> f64 {
  let image1 = args.value_of("image1").unwrap();
  let image2 = args.value_of("image2").unwrap();
  let path_string = format!("{}/banks/{}-{}", db_path, img_hash(image1), img_hash(image2));
  let path = Path::new(&path_string);
  let display = path.display();
  let sec_before;
  let sec_after;

  if !path.exists() {
    println!("I can't find requested pair bank {}.", display);
    println!("Have you run `sift pair-up {} {}` before this call?", image1, image2);
    return f64::NAN;
  }

  let mut file = match File::open(&path) {
    Err(why) => panic!("couldn't open {}: {}", display, why.description()),
    Ok(file) => file,
  };

  let mut bytes_in = Vec::new();
  match file.read_to_end(&mut bytes_in) {
    Err(why) => panic!("couldn't read {}: {}", display, why.description()),
    Ok(_) => {},
  }

  let pairs: Vec<(Point, Point)>;
  if let Some((result, _)) = unsafe { decode::<Vec<(Point, Point)>>(&mut bytes_in) } {
    pairs = result.clone();
  } else {
    println!("Decoding pair bank {} failed. It may be corrupted.", display);
    return f64::NAN;
  }

  let matches: Vec<(Point, Point)>;
  let conf_suffix = arg_hash();

  match args.value_of("method").unwrap() {
    "coherence" => {
      let order = usize::from_str(args.value_of("coherence_order").unwrap()).unwrap();
      let overlap_ratio_str = args.value_of("coherence_overlap").unwrap();
      let overlap_ratio = f64::from_str(overlap_ratio_str).unwrap();
      sec_before = time::precise_time_s();
      let coherent_pairs = sift::coherent_pairs(&pairs, order, (overlap_ratio * order as f64) as usize);
      sec_after = time::precise_time_s();
      println!("{} coherent pairs", coherent_pairs.len());
      matches = coherent_pairs;
    }
    "ransac" => {
      let threshold_str = args.value_of("ransac_threshold").unwrap();
      let threshold = f64::from_str(threshold_str).unwrap();
      let iterations = usize::from_str(args.value_of("ransac_iterations").unwrap()).unwrap();
      let mut rng = rand::XorShiftRng::from_seed(rand::random::<[u32; 4]>());
      let settings = sift::RansacSettings {
        model: match args.value_of("model") {
          Some("affine") => sift::RansacModel::Affine,
          Some("perspective") => sift::RansacModel::Perspective,
          m => panic!("unrecognized model: {:?}", m),
        },
        sampling_torus: if args.is_present("ransac_min_radius") || args.is_present("ransac_max_radius") {
          Some((f64::from_str(args.value_of("ransac_min_radius").unwrap()).unwrap(),
                f64::from_str(args.value_of("ransac_max_radius").unwrap()).unwrap()))
        } else {
          None
        },
      };
      sec_before = time::precise_time_s();
      let (ransac_pairs, dominance) = match sift::ransac(&pairs, threshold, iterations, settings, &mut rng) {
        Some((_, electorate, dominance)) => (electorate, dominance),
        None => {
          println!("RANSAC failed to achieve consensus; aborting");
          return f64::NAN;
        }
      };
      sec_after = time::precise_time_s();
      println!("{} pairs found by RANSAC (consensus dominance: {})", ransac_pairs.len(), dominance);
      matches = ransac_pairs;
    }
    method => panic!("unrecognized method: {:?}", method),
  }

  let mut bytes_out = Vec::new();
  unsafe { encode(&matches, &mut bytes_out); }
  {
    let path_string = format!("{}/matches-{}/{}-{}-{}", db_path, args.value_of("method").unwrap(), img_hash(image1), img_hash(image2), conf_suffix);
    let path = Path::new(&path_string);
    let display = path.display();

    let mut file = match File::create(&path) {
      Err(why) => panic!("couldn't create {}: {}",
                         display,
                         why.description()),
      Ok(file) => file,
    };

    match file.write_all(&bytes_out) {
      Err(why) => {
        panic!("couldn't write to {}: {}", display, why.description())
      },
      Ok(_) => {},  // println!("successfully wrote matchset {}", display),
    }
  }

  sec_after - sec_before
}
