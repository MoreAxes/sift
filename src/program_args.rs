use std::error::Error;
use std::str::FromStr;
use std::{f64, usize};

use clap::{App, Arg, SubCommand, AppSettings, ArgMatches};

fn validator<T: FromStr>(s: String) -> Result<(), String> where <T as FromStr>::Err: Error {
  match T::from_str(&s) {
    Ok(_) => Ok(()),
    Err(err) => Err(err.description().to_string()),
  }
}

fn common_args<'a>(app: App<'a, 'a>) -> App<'a, 'a> {
  app
  .arg_from_usage("<image1> 'First image'")
  .arg_from_usage("<image2> 'Second image'")
  .arg(Arg::with_name("method")
       .short("m")
       .long("method")
       .takes_value(true)
       .value_name("METHOD")
       .required(true)
       .possible_values(&["coherence", "ransac"])
       .help("use specified METHOD"))
  .arg(Arg::with_name("coherence_order")
       .long("coherence-order")
       .takes_value(true)
       .value_name("ORDER")
       .default_value("100")
       .validator(validator::<usize>)
       .help("examine ORDER nearest neighbours for overlap (coherence only)"))
  .arg(Arg::with_name("coherence_overlap")
       .long("coherence-overlap")
       .takes_value(true)
       .value_name("OVERLAP_RATIO")
       .default_value("0.5")
       .validator(validator::<f64>)
       .help("consider a pair coherent when OVERLAP_RATIO*ORDER neighbours match (coherence only)"))
  .arg(Arg::with_name("ransac_iterations")
       .long("ransac-iterations")
       .takes_value(true)
       .value_name("ITERATIONS")
       .default_value("1000")
       .validator(validator::<usize>)
       .help("iterate RANSAC this many times (ransac only)"))
  .arg(Arg::with_name("ransac_threshold")
       .long("ransac-threshold")
       .takes_value(true)
       .value_name("THRESHOLD")
       .default_value("10")
       .validator(validator::<f64>)
       .help("vote for model when pair error is less than THRESHOLD (ransac only)"))
  .arg(Arg::with_name("ransac_min_radius")
       .long("ransac-min-radius")
       .takes_value(true)
       .value_name("MIN_RADIUS")
       .validator(validator::<f64>)
       .help("minimum bounding radius for the RANSAC bounding heuristic"))
  .arg(Arg::with_name("ransac_max_radius")
       .long("ransac-max-radius")
       .takes_value(true)
       .value_name("MAX_RADIUS")
       .validator(validator::<f64>)
       .help("maximum bounding radius for the RANSAC bounding heuristic"))
  .arg(Arg::with_name("model")
       .short("d")
       .long("model")
       .takes_value(true)
       .value_name("MODEL")
       .possible_values(&["affine", "perspective"])
       .default_value("perspective")
       .help("use specified MODEL"))
}

pub fn get<'a>(default_extractor_path: &'a str) -> ArgMatches<'a> {
  App::new("sift-cmp")
  .author("Maciej Śniegocki, m.w.sniegocki@gmail.com")
  .version("0.0.1")
  .setting(AppSettings::SubcommandRequired)
  .setting(AppSettings::VersionlessSubcommands)
  .about("find matching components in pairs of images using sift features")
  .subcommand(SubCommand::with_name("pair-up")
              .about("compute mutual pair bank for an image pair and save it")
              .arg_from_usage("<image1> 'First image'")
              .arg_from_usage("<image2> 'Second image'")
              .arg(Arg::with_name("extractor_path")
                   .long("extractor-path")
                   .takes_value(true)
                   .value_name("EXTRACTOR_PATH")
                   .default_value(default_extractor_path)
                   .help("path to the feature extractor binary")))
  .subcommand(common_args(SubCommand::with_name("find-model").about("find matching components")))
  .subcommand(common_args(SubCommand::with_name("show").about("visualize computed model")))
  .subcommand(SubCommand::with_name("show-raw").about("visualize pair bank")
              .arg_from_usage("<image1> 'First image'")
              .arg_from_usage("<image2> 'Second image'"))
  .get_matches()
}