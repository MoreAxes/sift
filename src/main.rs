#![allow(dead_code)]

mod find_model;
mod pair_up;
mod program_args;
mod show;
mod sift;
mod util;

#[macro_use]
extern crate abomonation;
extern crate dotenv;
extern crate clap;
extern crate image;
extern crate kdtree;
extern crate nalgebra as na;
extern crate piston_window;
extern crate rand;
extern crate time;

use std::env;
use std::fs;
use std::path::Path;
use std::error::Error;

use dotenv::dotenv;

fn main() {
  dotenv().ok();

  let default_extractor_path = match env::var("SIFT_EXTRACTOR") {
    Ok(path) => path,
    Err(err) => panic!("reading env variable SIFT_EXTRACTOR failed: {}", err.description()),
  };

  let args = program_args::get(&default_extractor_path);

  let db_path = match env::var("SIFT_DB") {
    Ok(path) => path,
    Err(err) => {
      println!("reading env variable SIFT_DB failed: {}", err.description());
      return;
    }
  };
  if !Path::new(&db_path).exists() {
    if let Err(_) = fs::create_dir_all(&format!("{}/banks", db_path)) {
      println!("creating sift db failed; aborting");
      return
    }
    if let Err(_) = fs::create_dir_all(&format!("{}/matches-ransac", db_path)) {
      println!("creating sift db failed; aborting");
      return
    }
    if let Err(_) = fs::create_dir_all(&format!("{}/matches-coherence", db_path)) {
      println!("creating sift db failed; aborting");
      return
    }
  }

  let time_elapsed = match args.subcommand() {
    ("pair-up", submatches) => pair_up::go(&db_path, submatches.unwrap().clone()),
    ("find-model", submatches) => find_model::go(&db_path, submatches.unwrap().clone()),
    ("show", submatches) => { show::go(&db_path, false, submatches.unwrap().clone()); 0.0 },
    ("show-raw", submatches) => { show::go(&db_path, true, submatches.unwrap().clone()); 0.0 },
    _ => panic!("unrecognized subcommand"),
  };

  // println!("{:.*}s elapsed", 3, time_elapsed);
}
