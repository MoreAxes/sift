use std::iter::IntoIterator;
use std::env;
use image::*;

pub fn img_hash(path: &str) -> String {
  hash_stream(open(path).unwrap().raw_pixels())
}

pub fn hash_stream<I: IntoIterator<Item=u8>>(stream: I) -> String {
  // FNV-1a
  let mut hash: u64 = 14695981039346656037;
  for b in stream {
    hash ^= b as u64;
    hash *= 1099511628211;
  }

  format!("{:08x}", hash)
}

pub fn arg_hash() -> String {
  let mut hash = String::with_capacity(16);
  let mut relevant_args = env::args().skip(4).collect::<Vec<String>>();
  relevant_args.sort();
  for arg in relevant_args {
    hash = hash_stream(hash.bytes().chain(arg.bytes()));
  }
  hash
}