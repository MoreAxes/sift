use std::cmp::PartialEq;
use std::collections::HashSet;
use std::f64;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::iter::Iterator;
use std::mem;
use std::str::FromStr;

use abomonation::Abomonation;

use kdtree::KdTree;
use kdtree::distance::squared_euclidean;

use na;

use rand::Rng;
use rand;

pub struct Sift {
  pub pos: [f64; 2],
  pub features: [f64; 128],
}

impl PartialEq for Sift {
  fn eq(&self, other: &Sift) -> bool {
    self.pos == other.pos &&
    self.features.iter().zip(other.features.iter()).all(|(&s, &o)| s == o)
  }
}

impl Eq for Sift {}

impl Hash for Sift {
  fn hash<H: Hasher>(&self, state: &mut H) {
    unsafe { mem::transmute::<_, [u64; 2]>(self.pos) }.hash(state);
    Hash::hash(unsafe { &mem::transmute::<_, [u64; 128]>(self.features)[..] }, state);
  }
}

impl fmt::Debug for Sift {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    try!(write!(f, "Sift {{ x: {}, y: {}, {{ ", self.pos[0], self.pos[1]));
    for ft in self.features.iter() {
      try!(write!(f, "{}, ", ft))
    }
    try!(write!(f, "}}"));
    Ok(())
  }
}

impl Sift {
  pub fn zero() -> Sift {
    Sift {
      pos: [0.0; 2],
      features: [0.0; 128],
    }
  }

  pub fn point(&self) -> Point {
    Point {
      x: self.pos[0],
      y: self.pos[1],
    }
  }

  pub fn spatial_distance(&self, other: &Sift) -> f64 {
    let dx = self.pos[0] - other.pos[0];
    let dy = self.pos[1] - other.pos[1];
    (dx*dx + dy*dy).sqrt()
  }

  pub fn feature_distance(&self, other: &Sift) -> f64 {
    self.features.iter().zip(other.features.iter())
        .map(|(&s, &o)| s as f64 - o as f64)
        .fold(0.0, |acc, d| acc + d)
        .sqrt()
  }
}

impl FromStr for Sift {
  type Err = ();
  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let mut result = Sift::zero();
    let mut words = s.split_whitespace();
    result.pos[0] = f64::from_str(words.next().unwrap()).unwrap();
    result.pos[1] = f64::from_str(words.next().unwrap()).unwrap();
    // ignore next 3 values
    words.next();
    words.next();
    words.next();

    let mut features = 0usize;
    for (it, feat) in words.enumerate() {
      result.features[it] = f64::from_str(feat).unwrap();
      features += 1;
    }
    assert_eq!(features, 128);

    Ok(result)
  }
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct Point {
  pub x: f64,
  pub y: f64,
}

unsafe_abomonate!(Point : x, y);

impl Point {
  pub fn as_array(&self) -> &[f64; 2] {
    unsafe { mem::transmute::<&Point, &[f64; 2]>(self) }
  }

  pub fn sq_dist(&self, other: &Point) -> f64 {
    let dx = self.x - other.x;
    let dy = self.y - other.y;

    dx * dx + dy * dy
  }
}

impl Hash for Point {
  fn hash<H: Hasher>(&self, state: &mut H) {
    unsafe { mem::transmute::<_, [u64; 2]>(*self) }.hash(state);
  }
}

impl Eq for Point {}

pub fn parse_feature_set(filename: &str) -> Vec<Sift> {
  use std::error::Error;
  use std::fs::File;
  use std::io::prelude::*;
  use std::path::Path;
  use std::usize;

  let path = Path::new(filename);
  let display = path.display();
  let mut file = match File::open(&path) {
      Err(why) => panic!("couldn't open {}: {}", display, why.description()),
      Ok(file) => file,
  };

  let mut s = String::new();
  match file.read_to_string(&mut s) {
      Err(why) => panic!("couldn't read {}: {}", display, why.description()),
      Ok(_) => {},
  }

  let mut lines = s.lines();
  lines.next();  // skip sift length
  let feature_count = usize::from_str(lines.next().unwrap()).unwrap();
  let mut result = Vec::with_capacity(feature_count);
  result.extend(lines.map(|l| Sift::from_str(l).unwrap()));
  result
}

pub fn spatial_kdtree(bank: &[Sift]) -> KdTree<&Sift> {
  let mut result = KdTree::new_with_capacity(2, bank.len());
  for s in bank {
    result.add(&s.pos, s).unwrap();
  }
  result
}

pub fn feature_kdtree(bank: &[Sift]) -> KdTree<&Sift> {
  let mut result = KdTree::new_with_capacity(128, bank.len());
  for s in bank {
    result.add(&s.features, s).unwrap();
  }
  result
}

fn ptr_eq<T>(a: *const T, b: *const T) -> bool {
  a == b
}

pub fn raw_feature_pairs(bank1: &[Sift],
                         bank2: &[Sift]) -> Vec<(Point, Point)> {
  let ftree1 = feature_kdtree(bank1);
  let ftree2 = feature_kdtree(bank2);
  let mut result = Vec::new();
  for ft in bank1 {
    let nn: &Sift = ftree2.nearest(&ft.features, 1, &squared_euclidean).unwrap()[0].1;
    let reverse_nn: &Sift = ftree1.nearest(&nn.features, 1, &squared_euclidean).unwrap()[0].1;
    if ptr_eq(ft, reverse_nn) {
      result.push((ft.point(), nn.point()));
    }
  }
  result
}

pub fn coherent_pairs(pairs: &[(Point, Point)],
                      order: usize,
                      overlap: usize) -> Vec<(Point, Point)> {
  let mut stree1 = KdTree::new_with_capacity(2, pairs.len());
  let mut stree2 = KdTree::new_with_capacity(2, pairs.len());
  for s in pairs {
    stree1.add(s.0.as_array(), s.0).unwrap();
    stree2.add(s.1.as_array(), s.1).unwrap();
  }

  let mut nn1: HashSet<Point> = HashSet::new();
  let mut nn2: HashSet<Point> = HashSet::new();
  let mut nn1to2: HashSet<Point> = HashSet::new();
  let mut nn2to1: HashSet<Point> = HashSet::new();

  let mut result = Vec::new();

  result.extend(pairs.iter().filter(|&&(s1, s2)| {
    nn1.clear();
    nn2.clear();
    nn1to2.clear();
    nn2to1.clear();

    nn1.extend(stree1.nearest(s1.as_array(), order, &squared_euclidean).unwrap().iter().map(|t| *t.1));
    nn2.extend(stree2.nearest(s2.as_array(), order, &squared_euclidean).unwrap().iter().map(|t| *t.1));
    nn1to2.extend(nn1.iter().map(|s| pairs.iter().find(|&&(s1, _)| s1 == *s).expect("failed to pair point in 1-2 mapping").1));
    nn2to1.extend(nn2.iter().map(|s| pairs.iter().find(|&&(_, s2)| s2 == *s).expect("failed to pair point in 2-1 mapping").0));

    nn1to2.intersection(&nn2).count() >= overlap && nn2to1.intersection(&nn1).count() >= overlap
  }));

  result
}

pub fn affine_transform(pairs: &[(Point, Point)]) -> Option<na::Matrix3<f64>> {
  use na::Inverse;
  let x1 = pairs[0].0.x;
  let y1 = pairs[0].0.y;
  let x2 = pairs[1].0.x;
  let y2 = pairs[1].0.y;
  let x3 = pairs[2].0.x;
  let y3 = pairs[2].0.y;

  let u1 = pairs[0].1.x;
  let v1 = pairs[0].1.y;
  let u2 = pairs[1].1.x;
  let v2 = pairs[1].1.y;
  let u3 = pairs[2].1.x;
  let v3 = pairs[2].1.y;

  let xy_mat = na::Matrix6::new(
     x1,  y1, 1.0, 0.0, 0.0, 0.0,
     x2,  y2, 1.0, 0.0, 0.0, 0.0,
     x3,  y3, 1.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0,  x1,  y1, 1.0,
    0.0, 0.0, 0.0,  x2,  y2, 1.0,
    0.0, 0.0, 0.0,  x3,  y3, 1.0
  );
  let xy_mat_inv = match xy_mat.inverse() {
    Some(mat) => mat,
    None => return None,
  };

  let uv_vec = na::Vector6::new(u1, u2, u3, v1, v2, v3);

  let affine_vec = xy_mat_inv * uv_vec;

  Some(na::Matrix3::new(
    affine_vec[0], affine_vec[1], affine_vec[2],
    affine_vec[3], affine_vec[4], affine_vec[5],
              0.0,           0.0,           1.0
  ))
}

pub fn perspective_transform(pairs: &[(Point, Point)]) -> Option<na::Matrix3<f64>> {
  use na::Inverse;

  let x1 = pairs[0].0.x;
  let y1 = pairs[0].0.y;
  let x2 = pairs[1].0.x;
  let y2 = pairs[1].0.y;
  let x3 = pairs[2].0.x;
  let y3 = pairs[2].0.y;
  let x4 = pairs[3].0.x;
  let y4 = pairs[3].0.y;

  let u1 = pairs[0].1.x;
  let v1 = pairs[0].1.y;
  let u2 = pairs[1].1.x;
  let v2 = pairs[1].1.y;
  let u3 = pairs[2].1.x;
  let v3 = pairs[2].1.y;
  let u4 = pairs[3].1.x;
  let v4 = pairs[3].1.y;

  let mut mat = na::DMatrix::new_zeros(8, 8);

  mat[(0, 0)] = x1; mat[(0, 1)] = y1; mat[(0, 2)] = 1.0;
  mat[(1, 0)] = x2; mat[(1, 1)] = y2; mat[(1, 2)] = 1.0;
  mat[(2, 0)] = x3; mat[(2, 1)] = y3; mat[(2, 2)] = 1.0;
  mat[(3, 0)] = x4; mat[(3, 1)] = y4; mat[(3, 2)] = 1.0;

  mat[(4, 3)] = x1; mat[(4, 4)] = y1; mat[(4, 5)] = 1.0;
  mat[(5, 3)] = x2; mat[(5, 4)] = y2; mat[(5, 5)] = 1.0;
  mat[(6, 3)] = x3; mat[(6, 4)] = y3; mat[(6, 5)] = 1.0;
  mat[(7, 3)] = x4; mat[(7, 4)] = y4; mat[(7, 5)] = 1.0;

  mat[(0, 6)] = -u1 * x1; mat[(0, 7)] = -u1 * y1;
  mat[(1, 6)] = -u2 * x2; mat[(1, 7)] = -u2 * y2;
  mat[(2, 6)] = -u3 * x3; mat[(2, 7)] = -u3 * y3;
  mat[(3, 6)] = -u4 * x4; mat[(3, 7)] = -u4 * y4;

  mat[(4, 6)] = -v1 * x1; mat[(4, 7)] = -v1 * y1;
  mat[(5, 6)] = -v2 * x2; mat[(5, 7)] = -v2 * y2;
  mat[(6, 6)] = -v3 * x3; mat[(6, 7)] = -v3 * y3;
  mat[(7, 6)] = -v4 * x4; mat[(7, 7)] = -v4 * y4;

  let mut vec = na::DVector::new_zeros(8);

  vec[0] = u1;
  vec[1] = u2;
  vec[2] = u3;
  vec[3] = u4;
  vec[4] = v1;
  vec[5] = v2;
  vec[6] = v3;
  vec[7] = v4;

  if mat.inverse_mut() {
    let persp_vec = mat * vec;
    Some(na::Matrix3::new(
      persp_vec[0], persp_vec[1], persp_vec[2],
      persp_vec[3], persp_vec[4], persp_vec[5],
      persp_vec[6], persp_vec[7],          1.0
    ))
  } else {
    None
  }
}

pub fn model_error(model: na::Matrix3<f64>, (p1, p2): (Point, Point)) -> f64 {
  use na::Norm;

  let vec_to_transform = na::Vector3::new(p1.x, p1.y, 1.0);
  let transformed = model * vec_to_transform;
  let transformed_2d = na::Vector2::new(transformed.x, transformed.y);
  let other = na::Vector2::new(p2.x, p2.y);

  (transformed_2d - other).norm_squared()
}

pub fn new_pair_is_bounded<I: Iterator<Item=(Point, Point)>>(mut sample: I, (n1, n2): (Point, Point), min: f64, max: f64) -> bool {
  sample.all(|(p1, p2)| {
    let sq_dist_1 = p1.sq_dist(&n1);
    let sq_dist_2 = p2.sq_dist(&n2);
    min * min <= sq_dist_1 && sq_dist_1 <= max * max &&
    min * min <= sq_dist_2 && sq_dist_2 <= max * max
  })
}

pub enum RansacModel {
  Affine,
  Perspective,
}

pub struct RansacSettings {
  pub model: RansacModel,
  pub sampling_torus: Option<(f64, f64)>,
}

pub fn ransac<R: Rng>(pairs: &[(Point, Point)],
                      threshold: f64,
                      iterations: usize,
                      settings: RansacSettings,
                      rng: &mut R) -> Option<(na::Matrix3<f64>, Vec<(Point, Point)>, f64)> {
  let mut best_model = None;
  let mut best_score = 0;
  let mut best_electorate = Vec::new();
  let mut last_best_score = 0;
  for _ in 0..iterations {
    let mut model = None;
    let mut electorate = Vec::new();
    while let None = model {
      match settings.model {
        RansacModel::Affine => {
          let sample: Vec<(Point, Point)> = match settings.sampling_torus {
            None => rand::sample(rng, pairs, 3).iter().cloned().cloned().collect(),
            Some((min, max)) => {
              let mut sample = Vec::new();
              sample.push(rng.choose(pairs).unwrap().clone());
              let mut candidates = pairs.iter().cloned().filter(|&p| new_pair_is_bounded(sample.iter().cloned(), p, min, max)).collect::<Vec<(Point, Point)>>();
              sample.push(rng.choose(&candidates).unwrap().clone());
              candidates.retain(|&p| new_pair_is_bounded(sample.iter().cloned(), p, min, max));
              sample.push(rng.choose(&candidates).unwrap().clone());
              sample
            }
          };
          electorate.extend(sample.iter().cloned());
          model = affine_transform(&sample);
        }
        RansacModel::Perspective => {
          let sample: Vec<(Point, Point)> = match settings.sampling_torus {
            None => rand::sample(rng, pairs, 4).iter().cloned().cloned().collect(),
            Some((min, max)) => {
              let mut sample = Vec::new();
              sample.push(rng.choose(pairs).unwrap().clone());
              let mut candidates = pairs.iter().cloned().filter(|&p| new_pair_is_bounded(sample.iter().cloned(), p, min, max)).collect::<Vec<(Point, Point)>>();
              sample.push(rng.choose(&candidates).unwrap().clone());
              candidates.retain(|&p| new_pair_is_bounded(sample.iter().cloned(), p, min, max));
              sample.push(rng.choose(&candidates).unwrap().clone());
              candidates.retain(|&p| new_pair_is_bounded(sample.iter().cloned(), p, min, max));
              sample.push(rng.choose(&candidates).unwrap().clone());
              sample
            }
          };
          electorate.extend(sample.iter().cloned());
          model = perspective_transform(&sample);
        }
      }
    }
    let model = model.unwrap();
    let mut score = 0;
    for p in pairs {
      if model_error(model, *p) < threshold {
        score += 1;
        electorate.push(*p);
      }
    }
    if score > best_score {
      last_best_score = best_score;
      best_score = score;
      best_model = Some(model);
      best_electorate = electorate;
    }
  }

  let dominance = best_score as f64 / last_best_score as f64;
  best_model.map(|m| (m, best_electorate, dominance))
}