use sift::Point;
use util::*;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::u32;

use abomonation::decode;

use clap;

use piston_window::*;

pub fn go(db_path: &str, raw_pairs_only: bool, args: clap::ArgMatches) {
  let image1 = args.value_of("image1").unwrap();
  let image2 = args.value_of("image2").unwrap();
  let conf_suffix: String = arg_hash();
  let path_string = if !raw_pairs_only {
    format!("{}/matches-{}/{}-{}-{}", db_path, args.value_of("method").unwrap(), img_hash(image1), img_hash(image2), conf_suffix)
  } else {
    format!("{}/banks/{}-{}", db_path, img_hash(image1), img_hash(image2))
  };
  let path = Path::new(&path_string);
  let display = path.display();

  if !path.exists() {
    println!("I can't find requested matchset {}.", display);
    println!("Have you run `sift find-model --method={{coherence,ransac}} {} {}` before this call?", image1, image2);
    return;
  }

  let mut file = match File::open(&path) {
    Err(why) => panic!("couldn't open {}: {}", display, why.description()),
    Ok(file) => file,
  };

  let mut bytes_in = Vec::new();
  match file.read_to_end(&mut bytes_in) {
    Err(why) => panic!("couldn't read {}: {}", display, why.description()),
    Ok(_) => {},
  }

  let pairs: Vec<(Point, Point)>;
  if let Some((result, _)) = unsafe { decode::<Vec<(Point, Point)>>(&mut bytes_in) } {
    pairs = result.clone();
  } else {
    println!("Decoding matchset {} failed. It may be corrupted.", display);
    return;
  }

  println!("{} pairs.", pairs.len());

  let wwidth = 1800;
  let wheight = 900;
  let mut window: PistonWindow = WindowSettings::new("sift", (wwidth, wheight))
    .exit_on_esc(true)
    .samples(8)
    .build()
    .unwrap_or_else(|e| panic!("Failed to build PistonWindow: {}", e));

  let tex1 = Texture::from_path(&mut window.factory,
                                Path::new(image1),
                                Flip::None,
                                &TextureSettings::new()).unwrap();
  let img1_size = tex1.get_size();
  let img1 = Image::new().rect([0.0, 0.0, img1_size.0 as f64, img1_size.1 as f64]);

  let tex2 = Texture::from_path(&mut window.factory,
                                Path::new(image2),
                                Flip::None,
                                &TextureSettings::new()).unwrap();
  let img2_size = tex2.get_size();
  let img2 = Image::new().rect([img1_size.0 as f64, 0.0, img2_size.0 as f64, img2_size.1 as f64]);

  let bounding_rect = (img1_size.0 + img2_size.0, if img1_size.1 < img2_size.1 { img2_size.1 } else { img1_size.1 });
  let mut scale = if bounding_rect.0 > bounding_rect.1 {
    wwidth as f64 / bounding_rect.0 as f64
  } else {
    wheight as f64 / bounding_rect.1 as f64 / (wheight as f64 / wwidth as f64)
  };

  let mut translation = (0.0, 0.0);
  let mut dragging = false;

  let mut line_opacity = 0.25;
  let mut line = Line::new([1.0, 1.0, 1.0, line_opacity], 0.5);

  while let Some(e) = window.next() {
    if let Event::Input(ref inp) = e {
      match inp.clone() {
        Input::Press(Button::Mouse(MouseButton::Middle)) => dragging = true,
        Input::Release(Button::Mouse(MouseButton::Middle)) => dragging = false,
        Input::Move(Motion::MouseRelative(dx, dy)) => {
          if dragging {
            translation.0 += dx;
            translation.1 += dy;
          }
        }
        Input::Move(Motion::MouseScroll(dx, dy)) => {
          scale += (dx + dy) / 40.0;
          if scale < 0.05 {
            scale = 0.05;
          }
        }
        Input::Press(Button::Keyboard(Key::Z)) => {
          line_opacity -= 0.05;
          if line_opacity < 0.0 {
            line_opacity = 0.0;
          }
          line.color = [1.0, 1.0, 1.0, line_opacity];
        }
        Input::Press(Button::Keyboard(Key::X)) => {
          line_opacity += 0.05;
          if line_opacity > 1.0 {
            line_opacity = 1.0;
          }
          line.color = [1.0, 1.0, 1.0, line_opacity];
        }
        Input::Press(Button::Keyboard(Key::I)) => {
          scale = 1.0 / scale;
        }
        _ => {},
      }
    }

    line.radius = 0.5 / scale;

    window.draw_2d(&e, |_c, gl| {
      let _c = _c.trans(translation.0, translation.1).scale(scale, scale);
      clear([0.1, 0.1, 0.1, 1.0], gl);
      img1.draw(&tex1, &_c.draw_state, _c.transform, gl);
      img2.draw(&tex2, &_c.draw_state, _c.transform, gl);
      for &(ft1, ft2) in &pairs {
        let x1 = ft1.x;
        let y1 = ft1.y;
        let x2 = ft2.x + img1_size.0 as f64;
        let y2 = ft2.y;
        line.draw([x1, y1, x2, y2], &_c.draw_state, _c.transform, gl);
      }
    });
  }
}