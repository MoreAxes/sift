use sift;
use util::img_hash;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::process::Command;

use abomonation::encode;

use clap;

use time;

pub fn go(db_path: &str, args: clap::ArgMatches) -> f64 {
  let image1 = args.value_of("image1").unwrap();
  let image2 = args.value_of("image2").unwrap();

  let image1path = format!("{}.haraff.sift", image1);
  let image2path = format!("{}.haraff.sift", image2);

  if !Path::new(&image1path).exists() {
    let cmd1status = Command::new(args.value_of("extractor_path").unwrap())
        .arg("-haraff")
        .arg("-sift")
        .arg("-i")
        .arg(image1)
        .status()
        .unwrap_or_else(|e| panic!("failed to extract features: {}", e));
    if !cmd1status.success() {
      println!("Feature extraction failed (extractor exited with nonzero status code.");
    }
  }

  if !Path::new(&image2path).exists() {
    let cmd2status = Command::new(args.value_of("extractor_path").unwrap())
        .arg("-haraff")
        .arg("-sift")
        .arg("-i")
        .arg(image2)
        .status()
        .unwrap_or_else(|e| panic!("failed to extract features: {}", e));
    if !cmd2status.success() {
      println!("Feature extraction failed (extractor exited with nonzero status code.");
    }
  }

  println!("Features extracted; preparing data.");
  let feature_bank_1 = sift::parse_feature_set(&format!("{}.haraff.sift", image1));
  let feature_bank_2 = sift::parse_feature_set(&format!("{}.haraff.sift", image2));

  let sec_before = time::precise_time_s();
  let pairs = sift::raw_feature_pairs(&feature_bank_1, &feature_bank_2);
  let sec_after = time::precise_time_s();
  println!("{} pairs computed", pairs.len());

  let mut bytes = Vec::new();
  unsafe { encode(&pairs, &mut bytes); }
  let path_string = format!("{}/banks/{}-{}", db_path, img_hash(image1), img_hash(image2));
  let path = Path::new(&path_string);
  let display = path.display();

  let mut file = match File::create(&path) {
    Err(why) => panic!("couldn't create {}: {}", display, why.description()),
    Ok(file) => file,
  };

  match file.write_all(&bytes) {
    Err(why) => {
      panic!("couldn't write to {}: {}", display, why.description())
    },
    Ok(_) => println!("successfully wrote pair bank {}", display),
  }

  sec_after - sec_before
}